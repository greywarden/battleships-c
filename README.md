# Simple Battleships gane

## How to build
Tested only on Linux.
Generate Makefile from CMakeLists.txt:
```shell
cmake CMakeLists.txt
```
Then you can build it with make:
```shell
make
```
The output will be located in `bin` directory -> `Battleships`.

# Testing
After running `make` you can run tests by executing generated `BattleshipsTests`
located in `bin` directory. Output should be as follows:
```shell
>Executing HashSet test
Should contain entry in hash set - PASSED
Should add hash entry - PASSED
Should calculate hash - PASSED
Should return false for non existing element - PASSED
Should hashes be unique - PASSED
Should create new hash set - PASSED
---
>Executing GameBoard test
Should disallow overlay - PASSED
Should generate computer board - PASSED
Should can place ship with valid coords - PASSED
Should print board - PASSED
Should disallow placement outside of the board - PASSED
Should generate empty board - PASSED
Should deploy one ship - PASSED
---
>Executing GameController test
Should get next shot - PASSED
Should reject invalid shot - PASSED
Should get shots to go - PASSED
Should validate shot - PASSED
Should get next shot after invalid input - PASSED
Should perform shot - hit - PASSED
Should perform shot - miss - PASSED
Should create shot - PASSED
Should read n chars from stdin - PASSED
---
```
