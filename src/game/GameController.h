#ifndef BATTLESHIPS_C_GAMECONTROLLER_H
#define BATTLESHIPS_C_GAMECONTROLLER_H

struct shot {
    int x;
    int y;
    char *str;
};

enum shot_result {
    HIT,
    MISS
};

void start_game();

#endif
