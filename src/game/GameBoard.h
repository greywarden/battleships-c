#ifndef BATTLESHIPS_C_GAMEBOARD_H
#define BATTLESHIPS_C_GAMEBOARD_H

extern const char EMPTY_FIELD_MARK;
extern const char SHIP_MARK;
extern const int COMPUTER_SHIPS[];

char** generate_empty_board();
char** generate_computer_board();
void print_board(char**);

#endif
