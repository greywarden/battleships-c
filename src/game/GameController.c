#include "GameController.h"
#include "GameBoard.h"
#include "../collections/HashSet.h"
#include <regex.h>
#include <stddef.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

bool is_shot_valid(char *shot) {
    regex_t regex;
    regcomp(&regex, "^([a-jA-J])(([1-9])|(10))$", REG_EXTENDED);
    bool is_valid = !(regexec(&regex, shot, 0, NULL, 0));

    regfree(&regex);
    return is_valid;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err34-c"

struct shot create_shot(char *shot) {
    int x = toupper(shot[0]) - 'A';
    int y = atoi(shot + 1) - 1;

    char *shot_str = malloc(sizeof (char) * strlen(shot));
    strcpy(shot_str, shot);

    return (struct shot) {.x = x, .y = y, .str = shot_str};
}

#pragma clang diagnostic pop

enum shot_result perform_shoot(char **computer_board, char **player_board, struct shot shot) {
    if (computer_board[shot.x][shot.y] == SHIP_MARK) {
        computer_board[shot.x][shot.y] = 'H';
        player_board[shot.x][shot.y] = 'H';
        return HIT;
    } else {
        computer_board[shot.x][shot.y] = 'x';
        player_board[shot.x][shot.y] = 'x';
        return MISS;
    }
}

void read_n_chars_from_stdin(char *buffer, int limit) {
    int counter = 0;
    int c;
    while (c = getchar(), c != '\n' && c != EOF) {
        if (counter < limit) {
            buffer[counter] = (char) c;
            ++counter;
        }
    }
    buffer[counter] = '\0';
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-no-recursion"

struct shot get_next_shot() {
    char coords[5];
    printf("Enter coordinates: ");
    read_n_chars_from_stdin(coords, 4);

    if (is_shot_valid(coords)) {
        return create_shot(coords);
    } else {
        printf("#### Invalid coordinates, try again ####\n");
        return get_next_shot();
    }
}

#pragma clang diagnostic pop

int get_shots_to_go() {
    int shots_to_go = 0;
    for (int i = 0; i < 3; i++) {
        shots_to_go += COMPUTER_SHIPS[i];
    }
    return shots_to_go;
}

void start_game() {
    int shots_to_go = get_shots_to_go();
    HashSet previous_shots = new_hash_set();

    char **computer_board = generate_computer_board();
    char **player_board = generate_empty_board();
    print_board(computer_board);

    while (shots_to_go > 0) {
        struct shot _sh = get_next_shot();
        if (hash_set_contains(previous_shots, _sh.str)) {
            printf("You've already given those coordinates. Try again.\n");
            continue;
        }
        int shot_result = perform_shoot(computer_board, player_board, _sh);
        add_hash_entry(previous_shots, _sh.str);
        switch (shot_result) { // NOLINT(hicpp-multiway-paths-covered)
            case HIT:
                --shots_to_go;
                printf("Hit!\n");
                print_board(player_board);
                break;
            case MISS:
                printf("Miss!\n");
                print_board(player_board);
                break;
        }
        free(_sh.str);
    }
    printf("-- Game over --");
    print_board(computer_board);
    print_board(player_board);
    for (int i = 0; i < 10; i++) {
        free(computer_board[i]);
        free(player_board[i]);
    }
    free(player_board);
    free(computer_board);
}
