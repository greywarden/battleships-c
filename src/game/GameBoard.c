#include <malloc.h>
#include <stdlib.h>
#include <stdbool.h>
#include "GameBoard.h"

const char EMPTY_FIELD_MARK = '~';
const char SHIP_MARK = '#';
const char HIT_MARK = 'H';
const char MISS_MARK = 'x';

const int COMPUTER_SHIPS[] = {5, 4, 4};

char **generate_empty_board() {
    char **board = (char **) malloc(10 * sizeof(int *));
    for (int i = 0; i < 10; i++) {
        board[i] = (char *) malloc(10 * sizeof(char));
        for (int j = 0; j < 10; j++) {
            board[i][j] = EMPTY_FIELD_MARK;
        }
    }
    return board;
}

void print_board(char **board) {
    printf("   ");
    for (char c = 'A'; (unsigned) c < 'A' + 10; c++) {
        printf("%c ", c);
    }
    printf("\n");
    for (int i = 0; i < 10; i++) {
        printf("%2d ", i + 1);
        for (int j = 0; j < 10; j++) {
            printf("%c ", board[j][i]);
        }
        printf("\n");
    }
}

bool can_be_placed(char **board, int x, int y, int x1, int y1) {
    if (x1 >= 10 || y1 >= 10) {
        return false;
    }
    for (int i = x; i <= x1; i++) {
        for (int j = y; j <= y1; j++) {
            if (board[i][j] == SHIP_MARK) {
                return false;
            }
        }
    }

    return true;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-no-recursion"
#pragma ide diagnostic ignored "cert-msc50-cpp"

void deploy_ship(char **board, int ship) {
    bool horizontal = rand() & 1;

    int x = rand() % 10;
    int y = rand() % 10;

    int x1 = horizontal ? x : x + ship - 1;
    int y1 = horizontal ? y + ship - 1 : y;

    if (can_be_placed(board, x, y, x1, y1)) {
        for (int i = x; i <= x1; i++) {
            for (int j = y; j <= y1; j++) {
                board[i][j] = SHIP_MARK;
            }
        }
    } else {
        deploy_ship(board, ship);
    }
}

#pragma clang diagnostic pop

void deploy_ships(char **board) {
    for (int i = 0; i < 3; i++) {
        deploy_ship(board, COMPUTER_SHIPS[i]);
    }
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-msc51-cpp"


char **generate_computer_board() {
    srand(
#ifdef DEBUG
            42
#else
            time(NULL)
#endif
    );
    char **board = generate_empty_board();
    deploy_ships(board);
    return board;
}

#pragma clang diagnostic pop
