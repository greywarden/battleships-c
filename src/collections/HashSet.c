#include "HashSet.h"
#include <malloc.h>

unsigned long hash(const char* str) {
    unsigned long hash = 31;
    unsigned char c;

    while ((c = *str++)) {
        hash = ((hash << 5) + hash) + c;
    }
    return hash;
}

HashSet new_hash_set() {
    int initialSize = 100;
    HashSet hashSet = (HashSet) malloc(sizeof(HashSet));
    hashSet->size = initialSize;
    hashSet->entries = (char**) malloc(sizeof(char*) * initialSize);

    return hashSet;
}

void add_hash_entry(HashSet hashSet, char* entry) {
    unsigned long entryHash = hash(entry);
    hashSet->entries[entryHash % hashSet->size] = entry;
}

bool hash_set_contains(HashSet hashSet, char* entry) {
    unsigned long entryHash = hash(entry);
    return hashSet->entries[entryHash % hashSet->size] != NULL;
}
