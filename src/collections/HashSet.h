#ifndef BATTLESHIPS_C_HASHSET_H
#define BATTLESHIPS_C_HASHSET_H

#include <stdbool.h>

typedef struct hash_set* HashSet;

struct hash_set {
    char** entries;
    unsigned int size;
};

HashSet new_hash_set();
void add_hash_entry(HashSet, char*);
bool hash_set_contains(HashSet, char*);

#endif
