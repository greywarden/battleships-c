extern void hash_set_tests();
extern void game_board_tests();
extern void game_controller_tests();

int main(void) {
    hash_set_tests();
    game_board_tests();
    game_controller_tests();

    return 0;
}