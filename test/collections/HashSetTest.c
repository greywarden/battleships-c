#include <string.h>
#include "../../src/collections/HashSet.h"
#include "../common/UnitTest.h"

extern unsigned long hash(const char *);

void should_calculate_hash() {
    // given
    char *string = "test";

    // when
    unsigned long hash_value = hash(string);

    // then
    assert(hash_value == 41046143);
}

void should_hashes_be_unique() {
    // given
    char *string1 = "test";
    char *string2 = "test1";

    // when
    unsigned long hash_value_string1 = hash(string1);
    unsigned long hash_value_string2 = hash(string2);

    // then
    assert(hash_value_string1 != hash_value_string2);
}

void should_create_new_hash_set() {
    // when
    HashSet hs = new_hash_set();

    // then
    assert(hs->size == 100);
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            assert(hs->entries[i] == ((char *) 0));
        }
    }
    free(hs);
}

void should_add_hash_entry() {
    // given
    char *value = "test";
    unsigned long index = hash(value) % 100;
    HashSet hs = new_hash_set();

    // when
    add_hash_entry(hs, value);

    // then
    assert(strcmp(hs->entries[index], value) == 0);
    free(hs);
}

void should_contain_entry_in_hash_set() {
    // given
    char *entry = "test";
    HashSet hs = new_hash_set();

    // when
    add_hash_entry(hs, entry);

    // then
    assert(hash_set_contains(hs, entry));
    free(hs);
}

void should_return_false_for_non_existing_element() {
    // when
    char *entry = "non-existing";
    HashSet hs = new_hash_set();

    // then
    assert(!hash_set_contains(hs, entry));
    free(hs);
}

void hash_set_tests() {
    execute_test_cases("HashSet test",
                       6,
                       create_test_case(&should_calculate_hash, "Should calculate hash"),
                       create_test_case(&should_hashes_be_unique, "Should hashes be unique"),
                       create_test_case(&should_create_new_hash_set, "Should create new hash set"),
                       create_test_case(&should_add_hash_entry, "Should add hash entry"),
                       create_test_case(&should_contain_entry_in_hash_set, "Should contain entry in hash set"),
                       create_test_case(&should_return_false_for_non_existing_element, "Should return false for non existing element"));
}
