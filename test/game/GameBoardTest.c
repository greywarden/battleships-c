#include <unistd.h>
#include <string.h>
#include "../common/UnitTest.h"
#include <stdbool.h>
#include "../../src/game/GameBoard.h"

#define DEBUG

extern bool can_be_placed(char**, int, int, int, int);
extern void deploy_ship(char**, int);

void should_generate_empty_board() {
    // given
    char** board;

    // when
    board = generate_empty_board();

    // then
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            assert(board[i][j] == '~');
        }
        free(board[i]);
    }
    free(board);
}

void should_print_board() {
    // given
    char **board = generate_empty_board();
    board[0][0] = 'X';

    char buffer[512];
    int saved_stdout = dup(STDOUT_FILENO);
    int out_pipe[2];

    assert(pipe(out_pipe) == 0);
    dup2(out_pipe[1], STDOUT_FILENO);
    close(out_pipe[1]);

    char printed_board[263];
    const char *expected_board = "   A B C D E F G H I J "
                               "\n 1 X ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 2 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 3 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 4 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 5 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 6 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 7 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 8 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n 9 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ "
                               "\n10 ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ";
    // when
    print_board(board);
    fflush(stdout);

    // then
    read(out_pipe[0], buffer, 511);
    dup2(saved_stdout, STDOUT_FILENO);

    strncpy(printed_board, buffer, 263);

    assert(strcmp(printed_board, expected_board) == 0);

    for (int i = 0; i < 10; i++) {
        free(board[i]);
    }
    free(board);
}

void should_can_place_ship_with_valid_coords() {
    // given
    char **board = generate_empty_board();

    // when
    bool result = can_be_placed(board, 0, 0, 4, 0);

    // then
    assert(result);

    for (int i = 0; i < 10; i++) {
        free(board[i]);
    }
    free(board);
}

void should_disallow_placement_outside_of_the_board() {
    // given
    char **board = generate_empty_board();

    // when
    bool can_place_ship1 = can_be_placed(board, 8, 9, 8, 11);
    bool can_place_ship2 = can_be_placed(board, 8, 9, 11, 9);

    // then
    assert(!can_place_ship1);
    assert(!can_place_ship2);

    for (int i = 0; i < 10; i++) {
        free(board[i]);
    }
    free(board);
}

void should_disallow_overlay() {
     // given
    char **board = generate_empty_board();
    board[0][0] = SHIP_MARK;
    board[0][1] = SHIP_MARK;
    board[0][2] = SHIP_MARK;
    board[0][3] = SHIP_MARK;

    // when
    bool can_place_ship = can_be_placed(board, 0, 0, 3, 3);

    // then
    assert(!can_place_ship);

    for (int i = 0; i < 10; i++) {
        free(board[i]);
    }
    free(board);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-msc51-cpp"
void should_deploy_one_ship() {
    // given
    srand(42);
    char **board = generate_empty_board();

    // when
    deploy_ship(board, 4);

    // then
    assert(board[0][1] == '#');
    assert(board[1][1] == '#');
    assert(board[2][1] == '#');
    assert(board[3][1] == '#');

    for (int i = 0; i < 10; i++) {
        free(board[i]);
    }
    free(board);
}
#pragma clang diagnostic pop

void should_generate_computer_board() {
    // given
    char** board;

    // when
    board = generate_computer_board();

    // then
    assert(board[0][1] == '#');
    assert(board[1][1] == '#');
    assert(board[2][1] == '#');
    assert(board[3][1] == '#');
    assert(board[4][1] == '#');

    assert(board[0][5] == '#');
    assert(board[0][6] == '#');
    assert(board[0][7] == '#');
    assert(board[0][8] == '#');

    assert(board[4][3] == '#');
    assert(board[4][4] == '#');
    assert(board[4][5] == '#');
    assert(board[4][6] == '#');

    for (int i = 0; i < 10; i++) {
        free(board[i]);
    }
    free(board);
}

void game_board_tests() {
    execute_test_cases("GameBoard test",
                       7,
                       create_test_case(&should_generate_empty_board, "Should generate empty board"),
                       create_test_case(&should_print_board, "Should print board"),
                       create_test_case(&should_can_place_ship_with_valid_coords, "Should can place ship with valid coords"),
                       create_test_case(&should_disallow_placement_outside_of_the_board, "Should disallow placement outside of the board"),
                       create_test_case(&should_disallow_overlay, "Should disallow overlay"),
                       create_test_case(&should_deploy_one_ship, "Should deploy one ship"),
                       create_test_case(&should_generate_computer_board, "Should generate computer board"));
}

#undef DEBUG
