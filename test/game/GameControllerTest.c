#include "../common/UnitTest.h"
#include "../../src/game/GameController.h"
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

extern bool is_shot_valid(char *);
extern char **generate_empty_board();
extern struct shot create_shot(char *);
extern struct shot get_next_shot();
extern enum shot_result perform_shoot(char **, char **, struct shot);
extern void read_n_chars_from_stdin(char*, int);
extern int get_shots_to_go();

extern const char SHIP_MARK;
extern const char HIT_MARK;
extern const char MISS_MARK;

void should_validate_shot() {
    // given
    char *shot1 = "a1";
    char *shot2 = "j10";
    char *shot3 = "A10";
    char *shot4 = "C9";

    // when
    bool is_valid_first = is_shot_valid(shot1);
    bool is_valid_second = is_shot_valid(shot2);
    bool is_valid_third = is_shot_valid(shot3);
    bool is_valid_fourth = is_shot_valid(shot4);

    // then
    assert(is_valid_first);
    assert(is_valid_second);
    assert(is_valid_third);
    assert(is_valid_fourth);
}

void should_reject_invalid_shot() {
    // given
    char *shot1 = "a11";
    char *shot2 = "some-random-string";

    // when
    bool is_valid_first = is_shot_valid(shot1);
    bool is_valid_second = is_shot_valid(shot2);

    // then
    assert(!is_valid_first);
    assert(!is_valid_second);
}

void should_create_shot() {
    // given
    char shot[3] = {'c', '7', '\0'};

    // when
    struct shot result = create_shot(shot);

    // then
    assert(result.x == 2);
    assert(result.y == 6);
    assert(!strcmp(shot, "c7"));
}

void should_perform_shot_hit() {
    // given
    char **computer_board = generate_empty_board();
    char **player_board = generate_empty_board();
    computer_board[0][0] = SHIP_MARK;

    struct shot _sh = {.x = 0, .y = 0};

    // when
    enum shot_result result = perform_shoot(computer_board, player_board, _sh);

    // then
    assert(result == HIT);
    assert(computer_board[0][0] == HIT_MARK);
    assert(player_board[0][0] == HIT_MARK);

    for (int i = 0; i < 10; i++) {
        free(computer_board[i]);
        free(player_board[i]);
    }
    free(computer_board);
    free(player_board);
}

void should_perform_shot_miss() {
    // given
    char **computer_board = generate_empty_board();
    char **player_board = generate_empty_board();

    struct shot _sh = {.x = 0, .y = 0};

    // when
    enum shot_result result = perform_shoot(computer_board, player_board, _sh);

    // then
    assert(result == MISS);
    assert(computer_board[0][0] == MISS_MARK);
    assert(player_board[0][0] == MISS_MARK);

    for (int i = 0; i < 10; i++) {
        free(computer_board[i]);
        free(player_board[i]);
    }
    free(computer_board);
    free(player_board);
}

void should_read_n_chars_from_stdin() {
    // given
    int in_pipe[2];
    int saved_stdin = dup(STDIN_FILENO);
    char *input = "abc1234567890\n";
    int limit = 3;
    char buffer[64];

    assert(pipe(in_pipe) == 0);
    dup2(in_pipe[0], STDIN_FILENO);

    FILE *stdin_writer = fdopen(in_pipe[1], "w");
    fprintf(stdin_writer, "%s", input);
    fclose(stdin_writer);

    // when
    read_n_chars_from_stdin(buffer, limit);

    // then
    assert(!strcmp(buffer, "abc"));

    dup2(saved_stdin, STDIN_FILENO);
}

void should_get_next_shot() {
    // given
    int in_pipe[2];
    int saved_stdin = dup(STDIN_FILENO);
    char *input = "a3\n";

    int saved_stdout = dup(STDOUT_FILENO);
    int out_pipe[2];

    assert(pipe(in_pipe) == 0);
    dup2(in_pipe[0], STDIN_FILENO);
    assert(pipe(out_pipe) == 0);
    dup2(out_pipe[1], STDOUT_FILENO);
    close(out_pipe[1]);

    FILE *stdin_writer = fdopen(in_pipe[1], "w");
    fprintf(stdin_writer, "%s", input);
    fclose(stdin_writer);

    // when
    struct shot _sh = get_next_shot();

    // then
    assert(_sh.x == 0);
    assert(_sh.y == 2);

    fflush(stdout);
    dup2(saved_stdin, STDIN_FILENO);
    dup2(saved_stdout, STDOUT_FILENO);
}

void should_get_next_shot_after_invalid_input() {
    // given
    int in_pipe[2];
    int saved_stdin = dup(STDIN_FILENO);
    char *input = "x55\na3\n";

    int saved_stdout = dup(STDOUT_FILENO);
    int out_pipe[2];

    assert(pipe(in_pipe) == 0);
    dup2(in_pipe[0], STDIN_FILENO);
    assert(pipe(out_pipe) == 0);
    dup2(out_pipe[1], STDOUT_FILENO);
    close(out_pipe[1]);

    FILE *stdin_writer = fdopen(in_pipe[1], "w");
    fprintf(stdin_writer, "%s", input);
    fclose(stdin_writer);

    // when
    struct shot _sh = get_next_shot();

    // then
    assert(_sh.x == 0);
    assert(_sh.y == 2);

    fflush(stdout);
    dup2(saved_stdin, STDIN_FILENO);
    dup2(saved_stdout, STDOUT_FILENO);

}

void should_get_shots_to_go() {
    // given
    int expected = 13;

    // when
    int actual = get_shots_to_go();

    // then
    assert(actual == expected);
}

void game_controller_tests() {
    execute_test_cases("GameController test",
                       9,
                       create_test_case(&should_validate_shot, "Should validate shot"),
                       create_test_case(&should_reject_invalid_shot, "Should reject invalid shot"),
                       create_test_case(&should_create_shot, "Should create shot"),
                       create_test_case(&should_perform_shot_hit, "Should perform shot - hit"),
                       create_test_case(&should_perform_shot_miss, "Should perform shot - miss"),
                       create_test_case(&should_read_n_chars_from_stdin, "Should read n chars from stdin"),
                       create_test_case(&should_get_next_shot, "Should get next shot"),
                       create_test_case(&should_get_next_shot_after_invalid_input, "Should get next shot after invalid input"),
                       create_test_case(&should_get_shots_to_go, "Should get shots to go"));
}
