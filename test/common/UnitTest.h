#ifndef BATTLESHIPS_C_UNITTEST_H
#define BATTLESHIPS_C_UNITTEST_H

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

struct test_case {
    void (*test)();
    const char *test_name;
};

static void swap(struct test_case *a, struct test_case *b) {
    struct test_case temp = *a;
    *a = *b;
    *b = temp;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-msc51-cpp"
#pragma ide diagnostic ignored "cert-msc50-cpp"

static void shuffle_array(struct test_case *test_cases, int n) {
    srand(time(NULL));
    for (int i = n - 1; i > 0; i--) {
        int j = rand() % (i + 1);
        swap(&test_cases[i], &test_cases[j]);
    }
}

#pragma clang diagnostic pop

static struct test_case create_test_case(void (*test)(), const char *test_name) {
    return (struct test_case) {.test = test, .test_name = test_name};
}

static void execute_test_cases(const char *test_suite, int n, ...) {
    printf(">Executing %s\n", test_suite);
    struct test_case test_cases[n];

    va_list argp;
    va_start(argp, n);
    for (int i = 0; i < n; i++) {
        test_cases[i] = va_arg(argp, struct test_case);
    }
    va_end(argp);

    shuffle_array(test_cases, n);
    for (int i = 0; i < n; i++) {
        printf("%s - \033[1;32mPASSED\033[0m\n", test_cases[i].test_name);
        test_cases[i].test();
    }
    printf("---\n");
}

#endif
